export const environment = {
  production: true,
  firebaseConfig: {
    projectId: 'add-tech-fe',
    appId: '1:930597067234:web:923da9cebc7d992b7f41bb',
    databaseURL: 'https://add-tech-fe-default-rtdb.firebaseio.com',
    storageBucket: 'add-tech-fe.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyCa2InUgoC0f2IAD4YY1RsprLf0uzf4-7E',
    authDomain: 'add-tech-fe.firebaseapp.com',
    messagingSenderId: '930597067234',
    measurementId: 'G-NRP6NCR2T3',
  },
};
