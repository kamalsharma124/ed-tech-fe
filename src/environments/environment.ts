// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    projectId: 'add-tech-fe',
    appId: '1:930597067234:web:923da9cebc7d992b7f41bb',
    databaseURL: 'https://add-tech-fe-default-rtdb.firebaseio.com',
    storageBucket: 'add-tech-fe.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyCa2InUgoC0f2IAD4YY1RsprLf0uzf4-7E',
    authDomain: 'add-tech-fe.firebaseapp.com',
    messagingSenderId: '930597067234',
    measurementId: 'G-NRP6NCR2T3',
  },
};
