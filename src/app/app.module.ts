
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import {MatIconModule} from '@angular/material/icon';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { AgmCoreModule } from '@agm/core';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {firebase, firebaseui, FirebaseUIModule, FirebaseuiAngularLibraryService} from 'firebaseui-angular';
import {AngularFireModule} from '@angular/fire/compat';
import {AngularFireAuthModule, USE_EMULATOR as USE_AUTH_EMULATOR} from '@angular/fire/compat/auth';
import { environment } from '../environments/environment';
import { SignInComponent } from './sign-in/sign-in.component';


const firebaseUiAuthConfig: firebaseui.auth.Config = {
  // signInFlow: 'popup',
  // signInOptions: [
  //   firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  //   {
  //     scopes: [
  //       'public_profile',
  //       'email',
  //       'user_likes',
  //       'user_friends'
  //     ],
  //     customParameters: {
  //       'auth_type': 'reauthenticate'
  //     },
  //     provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID
  //   },
  //   // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
  //   // firebase.auth.GithubAuthProvider.PROVIDER_ID,
  //   {
  //     requireDisplayName: false,
  //     provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
  //   },
  //   firebase.auth.PhoneAuthProvider.PROVIDER_ID,
  //   firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  // ],
  // // tosUrl: '<your-tos-link>',
  // // privacyPolicyUrl: '<your-privacyPolicyUrl-link>',
  // credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO
};

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    MatCardModule,
    MatToolbarModule,
    MatIconModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    SignInComponent,

  ],
  providers: [
    { provide: USE_AUTH_EMULATOR, useValue: !environment.production ? ['localhost', 9099] : undefined },
{
  provide: 'appConfig',
  useValue: { googleAuthEnabled: true, emailAuthEnabled: true, facebookAuthEnabled: true, PhoneAuthEnabled: true, AnonymousAuthEnabled: true }
},
{
    provide: 'firebaseUIAuthConfig',
    useFactory: (config) => {

      // build firebase UI config object using settings from `config`

      const fbUiConfig: firebaseui.auth.Config = {
        // signInSuccessUrl: 'https://www.google.com',
        signInFlow: 'popup',
        signInOptions: [
        ],
        tosUrl: '<your-tos-link>',
         privacyPolicyUrl: '<your-privacyPolicyUrl-link>',
        credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
      };

      if (config.googleAuthEnabled) {
        fbUiConfig.signInOptions.push({
          provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          requireDisplayName: true,
          fullLabel: 'Google',
        });
      }

      if (config.emailAuthEnabled) {
        fbUiConfig.signInOptions.push({
          provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
          // requireDisplayName: false,
          fullLabel: 'Email',
          signInMethod: firebase.auth.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD
        });
      }

      if (config.facebookAuthEnabled) {
        fbUiConfig.signInOptions.push({
          provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
          fullLabel: 'Facebook',
          requireDisplayName: true,
        });
      }

      if (config.PhoneAuthEnabled) {
        fbUiConfig.signInOptions.push({
          provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
          fullLabel: 'Phone',
          requireDisplayName: true,
        });
      }

      if (config.AnonymousAuthEnabled) {
        fbUiConfig.signInOptions.push({
        provider: firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID,
        fullLabel: 'Guest',
        requireDisplayName: true,
      });
      }
      return fbUiConfig;
    },
    deps: ['appConfig']
  }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
