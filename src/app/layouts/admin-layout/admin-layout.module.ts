import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {firebase, firebaseui, FirebaseUIModule} from 'firebaseui-angular';

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  // signInFlow: 'popup',
  // signInOptions: [
  //   firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  //   {
  //     scopes: [
  //       'public_profile',
  //       'email',
  //       'user_likes',
  //       'user_friends'
  //     ],
  //     customParameters: {
  //       'auth_type': 'reauthenticate'
  //     },
  //     provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID
  //   },
  //   firebase.auth.TwitterAuthProvider.PROVIDER_ID,
  //   firebase.auth.GithubAuthProvider.PROVIDER_ID,
  //   {
  //     requireDisplayName: false,
  //     provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
  //   },
  //   firebase.auth.PhoneAuthProvider.PROVIDER_ID,
  //   firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  // ],
  // tosUrl: '<your-tos-link>',
  // privacyPolicyUrl: '<your-privacyPolicyUrl-link>',
  // credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    // FirebaseUIModule.forRoot(firebaseUiAuthConfig)
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
  ]
})

export class AdminLayoutModule {}
